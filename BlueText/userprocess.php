<?php

session_start();

require_once "dao.php";
$dao = new Dao();

//Define variables
$tokenErr="";
//$token="yeah";


if(isset($_POST["submit"])&& $dao->hasPermission($_POST["tokenForm"])!=null){

   $output=$dao->hasPermission($_POST["tokenForm"]);
   $_SESSION["permission"]=1;
   $_SESSION["token"]=$output["token"];
   $_SESSION["regid"]=$output["regid"];
   header("Location: yourIn.php");
}
else{
   $_SESSION["tokenErr"] = "Incorrect Token";
   header("Location: login.php");
}

if(isset($_POST["token"])){
   $dao->insertTok($_POST["token"], $_POST["regid"]);
}

