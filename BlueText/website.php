<html>
	<head>
		<title>Crow's CS354 Ruby Tutorial</title>
		<link rel="stylesheet" type="text/css" href="website.css"/>
      <link rel="stylesheet" type="text/css" href="/BlueText/css/main2.css">
		<link rel="shortcut icon" href="./favicon.ico"/>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script>
			$(document).ready(function(){
				$('ul.tabs').each(function(){
					var $active, $content, $links = $(this).find('a');

					$active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
					$active.addClass('active');
					$content = $($active.attr('href'));

					$links.not($active).each(function () {
						$($(this).attr('href')).hide();
					});

					$(this).on('click', 'a', function(e){
						$active.removeClass('active');
						$content.hide();

						$active = $(this);
						$content = $($(this).attr('href'));

						$active.addClass('active');
						$content.show();

						e.preventDefault();
					});
				});
			});
		</script>

	</head>
	
	<body>
	<div id ='tabs'>
	<h1 id='banner'> Crow's Ruby Website </h1>
	 <ul class='tabs'>
    <li><a href='#descAndHist'>Description and History</a></li>
    <li><a href='#helpfulLinks'>Helpful Links</a></li>
    <li><a href='#transAndInstall'>Translators and Installation</a></li>
    <li><a href='#introPrograms'>Intro Programs</a></li>
    <li><a href='#complexProgs'>Complex Programs</a></li>
    <li><a href='#compare'>How does Ruby compare?</a></li>
  </ul>

 <div id ='descAndHist'>
		<h1>Description and History</h1>
		
		<h2>Description</h2>
		<p>TODO
		
		<h2>History</h2>
		<p>TODO<br>Ruby was developed in the mid 1990s by Yukihiro Matsumoto in Japan
		It was designed to be a flexible and powerful language. Yukihiro designed
		it because he wanted a scripting language that was more powerful than Perl
		and more object-oriented than Python.<sup><a href="http://www.linuxdevcenter.com/pub/a/linux/2001/11/29/ruby.html">1</a></sup>
	</div>
	<div id='helpfulLinks'>
		<h1>Helpful Links</h1>
		
		<p>TODO<br>
		<a href="https://ruby-lang.org/en/documentation/quickstart/">Another Ruby tutorial</a><br>
		<a href="http://rubykoans.com/">Ruby Koans</a>
	</div>
	<div id='transAndInstall'>
		<h1>Available Translators and Installation</h1>
		<p>TODO
	</div>
	<div id='introPrograms'>
		<h1>Introductory Programs</h1>
		<p>TODO

		<h3>Hello, World!</h3>
		
		<p>Let's start with a basic Hello, World! program.<br>
		Simply write this into a file and run it.
		
		<?php
			$string = file_get_contents('./helloWorld.rb',true);
			echo "<div class='code'><pre><code>" . $string . "</code></pre></div>";
		?>
		
		<p>Running this input will produce "Hello, World!"<br>
		Congratulations! You just finished your first Ruby program.<br>
		A few important things to note:
		<p class="tab"> <ul>
		<li>The def keyword always begins a function.</li>
		<li>The end keyword always ends a function. </li>
		<li>puts is a predefined function to write to the console.</li>
		</ul>
				
		<h3>Fibonacci</h3>
		<p>Let's look at an example with some basic recursion
		<?php
			$string = file_get_contents('./fibonacci.rb',true);
			echo "<div class='code'><pre><code>" . $string . "</code></pre></div>";
		?>
		<h3>Sum</h3>
		<?php
			$string = file_get_contents('./sum.rb',true);
			echo "<div class='code'><pre><code>" . $string . "</code></pre></div>";
		?>
		<h3>Lists</h3>
		<?php
			$string = file_get_contents('./list.rb',true);
			echo "<div class='code'><pre><code>" . $string . "</code></pre></div>";
		?>
		<h3>Console I/O</h3>
		<?php
			$string = file_get_contents('./consoleIO.rb',true);
			echo "<div class='code'><pre><code>" . $string . "</code></pre></div>";
		?>
	</div>
	<div id='complexProgs'>
		<h1>More Complex Programs</h1>
		<p>Smalltalk program
		
		<p>Bank
		<?php
			$string = file_get_contents('./Bank.rb',true);
			echo "<div class='code'><pre><code>" . $string . "</code></pre></div>";
		?>
		<p>Account
		<?php
			$string = file_get_contents('./Account.rb',true);
			echo "<div class='code'><pre><code>" . $string . "</code></pre></div>";
		?>
		<p>Checking Account
		<?php
			$string = file_get_contents('./checkingAccount.rb',true);
			echo "<div class='code'><pre><code>" . $string . "</code></pre></div>";
		?>
		<p>Savings Account
		<?php
			$string = file_get_contents('./savingsAccount.rb',true);
			echo "<div class='code'><pre><code>" . $string . "</code></pre></div>";
		?>
		<p>Customer
		<?php
			$string = file_get_contents('./customer.rb',true);
			echo "<div class='code'><pre><code>" . $string . "</code></pre></div>";
		?>
	
		<p>Some examples of Ruby's lambda functions
		<?php
			$string = file_get_contents('./lambdas.rb',true);
			echo "<div class='code'><pre><code>" . $string . "</code></pre></div>";
		?>
	</div>
	<div id='compare'>
		<h1>How Does Ruby Compare to Other Languages?</h1>
		<table>
			<tr>
			<th></th>
			<th>Ruby</th>
			<th>Java</th>
			<th>C</th>
			<th>Prolog</th>
			</tr>
			<tr>
				<td class='category'>Language Type</td> <td>Scripting</td> <td>Object Oriented</td> <td>VonNeuman</td> <td>Functional</td> <td>Logic</td>
			</tr>
			<tr>
				<td class='category'>Compiled/Interpreted</td> <td>Interpreted</td> <td>Both</td> <td>Compiled</td> <td>Interpreted</td> <td>Interpreted</td>
			</tr>
			<tr>
				<td class='category'>Object Oriented</td> <td>yes</td> <td>yes</td> <td>no</td> <td>no</td> <td>no</td>
			</tr>
			<tr>
				<td class='category'>Specify Variable Types</td><td>no</td><td>yes</td><td>yes</td><td>no</td><td>no</td>
			</tr>
			<tr>
				<td class='category'>Explicit Pointers</td> <td>no</td> <td>no</td> <td>yes</td> <td>no</td> <td>no</td>
			</tr>
			<tr>	
				<td class='category'>Automatic Memory Management</td> <td>yes</td> <td>yes</td> <td>no</td> <td>yes</td> <td>yes</td>
			</tr>
		</table>
	</div>
</div>
	</body>
</html>
