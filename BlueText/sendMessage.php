<?php
session_start();
require_once'dao.php';
require_once'config.php';
include_once './gcmPost.php';
require_once'sqDB.php';

$db = new DynamicDb($_SESSION["token"]); 
$dao = new Dao();

 function micro(){
   list($usec, $sec) = explode(" ", microtime());
   return((float) $usec + (float)$sec);
}

if (isset($_POST["submit"])) {
   $db->exec("INSERT INTO messages (body , dateSent, address, type) 
      VALUES('" . $_POST["send"]."','". micro()*1000 . "','" . $_SESSION['number']."', 'sent')");

   $gcm = new GCM();

   $registration_ids = array($_SESSION["regid"]);
   $message = array("message" => $_POST["send"],
      "phone"=> $_SESSION["number"]);
   $result = $gcm->send_notification($registration_ids, $message);
   header("Location: yourIn.php?id=".$_SESSION["num"]);
}

if (isset($_POST["submitNew"])) {
   $phone = $_POST["phoneNum"];

   $db->exec("INSERT INTO messages (body , dateSent, address, type) 
      VALUES('" . $_POST["sendNew"]."','". micro()*1000 . "','" . $phone."', 'sent')");

   $gcm = new GCM();

   $registration_ids = array($_SESSION["regid"]);
   $message = array("message" => $_POST["sendNew"],
      "phone"=> $phone);
   $result = $gcm->send_notification($registration_ids, $message);
   header("Location: yourIn.php");

}
